<?php

$page_title = 'Error '.$code.' '.$message;

require 'base.php';

$start_template();

?>
<p><?= $code ?>. That's an error.</p>
<?php
  if ($explaination) {
    echo '<p>'.htmlspecialchars($explaination).'</p>';
  }
?>
<p>That's all we know.</p>
<?php

$end_template();
