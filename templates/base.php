<?php

$start_template = function() use($page_title) {

$favicon_fc = 't1mkkQI';
$style_main_fc = '5HEO09a';
$style_noscript_fc = 'GPM3O3h';

if (!isset($page_title)) {
  $page_title = 'Ymage';
}

?><!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= htmlspecialchars($page_title) ?></title>

    <meta name="theme-color" content="#FFFFFF">
    <meta name="msapplication-TileColor" content="#DFFFFF">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png?fc=<?= $favicon_fc ?>">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?fc=<?= $favicon_fc ?>" color="#FFFFFF">
    <link rel="shortcut icon" href="/favicon.ico?fc=<?= $favicon_fc ?>">
    <link rel="icon" href="/favicon-16x16.png?fc=<?= $favicon_fc ?>" sizes="16x16">
    <link rel="icon" href="/favicon-32x32.png?fc=<?= $favicon_fc ?>" sizes="32x32">
    <link rel="icon" href="/android-chrome-192x192.png?fc=<?= $favicon_fc ?>" sizes="192x192">
    <link rel="icon" href="/android-chrome-512x512.png?fc=<?= $favicon_fc ?>" sizes="512x512">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png?fc=<?= $favicon_fc ?>">

    <link rel="stylesheet" href="/assets/css/main.css?fc=<?= $style_main_fc ?>">
    <noscript><link rel="stylesheet" href="/assets/css/noscript.css?fc=<?= $style_noscript_fc ?>"></noscript>

    <script>document.addEventListener('touchstart', function() {})</script>
  </head>
  <body>
    <header class="site-header">
      <a class="site-name" href="/">
        <img class="site-logo" src="/android-chrome-192x192.png?fc=<?= $favicon_fc ?>" alt="Logo">
        Ymage
      </a>
      <nav class="navbar">
        <div id="navbar-button" aria-role="button" aria-haspopup="true"><i class="material-icons">menu</i></div>
        <ul id="navbar-items" aria-labelledby="navbar-button">
          <li><a href="/"><i class="material-icons">file_upload</i>&nbsp;Upload</a></li>
          <li><a href="/api"><i class="material-icons">explore</i>&nbsp;API</a></li>
          <li><a href="/about"><i class="material-icons">info_outline</i>&nbsp;About</a></li>
        </ul>
      </nav>
    </header>
    <div class="page-content">
      <?php

};

$end_template = function() {

$jquery_fc = 'GaOJ4P7';

      ?>
    </div>
    <script src="/node_modules/jquery/dist/jquery.min.js?fc=<?= $jquery_fc ?>"></script>
  </body>
</html>
<?php

};
