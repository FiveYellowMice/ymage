<?php

require 'base.php';

$start_template();

?>
<h1><?= htmlspecialchars($page_title) ?></h1>
<?= $page_content ?>
<?php

$end_template();
