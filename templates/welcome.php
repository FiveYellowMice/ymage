<?php

$page_title = 'Ymage';

require 'base.php';

$start_template();

?>
<h1 class="welcome-title">Ymage</h1>
<p class="welcome-instruction">
  Select your file here:
</p>
<form class="upload-form" enctype="multipart/form-data" method="post">
  <label id="upload-file-selector" class="upload-button">
    <span class="noscript-hidden">Select File</span>
    <input type="file" name="file">
  </label>
  <button type="submit" id="upload-submit" class="upload-button">Upload File</button>
</form>

<p class="upload-error"<?php if (!$upload_error): ?> style="display: none" aria-hidden="true"<?php endif; ?>>
  <?= htmlspecialchars($upload_error) ?>
</p>
<p class="upload-success"<?php if (!$upload_result_url_long || !$upload_result_url_short): ?> style="display: none" aria-hidden="true"<?php endif; ?>>
  The file has been uploaded successfuly. Both of the following URLs will point to that file.
</p>
<p class="upload-result-url-short upload-result-url"<?php if (!$upload_result_url_short): ?> style="display: none" aria-hidden="true"<?php endif; ?>>
  <a href="<?= htmlspecialchars($upload_result_url_short) ?>"><?= htmlspecialchars($upload_result_url_short) ?></a>
</p>
<p class="upload-result-url-long upload-result-url"<?php if (!$upload_result_url_long): ?> style="display: none" aria-hidden="true"<?php endif; ?>>
  <a href="<?= htmlspecialchars($upload_result_url_long) ?>"><?= htmlspecialchars($upload_result_url_long) ?></a>
</p>
<p class="upload-converter-instruction"<?php if (!$upload_result_url_long): ?> style="display: none" aria-hidden="true"<?php endif; ?>>
  You can convert the image to other formats by appending <code>.png</code>, <code>.jpg</code>, <code>.gif</code> or <code>.webp</code> to the URL.
</p>

<script>
document.addEventListener('DOMContentLoaded', function() {
  $('#upload-submit').attr('aria-hidden', true)

  $('#upload-file-selector input').change(function() {
    $('#upload-submit').click()
  })

  $('.upload-converter-instruction code').each(function(i, el) {
    var $el = $(el)
    var extension = $el.text()
    $el.html('')
    $el.append(
      $('<a>')
      .attr('aria-role', 'button')
      .attr('href', 'javascript:void(0)')
      .text(extension)
      .click(function() {
        function changeUrlInElement(changingElement) {
          var originalUrl = changingElement.attr('data-original-url')
          if (!originalUrl) {
            originalUrl = changingElement.text()
            changingElement.attr('data-original-url', originalUrl)
          }
          changingElement.text(originalUrl + extension)
          changingElement.attr('href', originalUrl + extension)
        }
        changeUrlInElement($('.upload-result-url-short>a'))
        changeUrlInElement($('.upload-result-url-long>a'))
        $('.upload-converter-instruction-revert-button').show().attr('aria-hidden', false)
      })
    )
  })

  $('.upload-converter-instruction').append(
    $('<a href="javascript:void(0)" aria-role="button" style="display: none" aria-hidden="true">Revert</a>')
    .addClass('upload-converter-instruction-revert-button')
    .click(function(event) {
      function revertUrlInElement(revertingElement) {
        var originalUrl = revertingElement.attr('data-original-url')
        if (!originalUrl) {
          return
        }
        revertingElement.text(originalUrl)
        revertingElement.attr('href', originalUrl)
      }
      revertUrlInElement($('.upload-result-url-short>a'))
      revertUrlInElement($('.upload-result-url-long>a'))
      $(event.target).hide().attr('aria-hidden', true)
    })
  )
})
</script>
<?php

$end_template();
