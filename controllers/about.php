<?php

require_once '../lib/return_error.php';

if (!in_array($_SERVER['REQUEST_METHOD'], ['HEAD', 'GET'])) {
  return_error(405, 'Method Not Allowed', 'You can not use '.$_SERVER['REQUEST_METHOD'].' method on this page.');
}

$page_title = 'About';

$page_content = "
<p>Ymage is a free and public image pastebin powered by <a rel=\"nofollow\" href=\"https://php.net\">the best programming language</a>.</p>
<p>By using this site, you agree that you are aware and understand the following terms and conditions:</p>
<ol>
  <li>The site owner does not take any responsibility of any content posted by user on this site.</li>
  <li>You will only upload images you have right to upload.</li>
  <li>Any content posted on this site may be remove unconditionally, without any notice or explaination.</li>
  <li>Your ability to use this service is a privilege, not a right. The site owner reserves the right to stop the service to anyone.</li>
  <li>The availability of this site is not guaranteed.</li>
  <li>You will not abuse this service, including but not limited to: mass uploading, unethical crawling, trying to get administrative access of the server and using the service for any illegal activities.</li>
</ol>
<p>Further, <a href=\"https://gitlab.com/FiveYellowMice/ymage\">the software</a> used by this site is released under the MIT license.</p>
";

header('Cache-Control: no-cache');
require '../templates/static_page.php';
