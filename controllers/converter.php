<?php

require_once '../lib/return_error.php';

if (!in_array($_SERVER['REQUEST_METHOD'], ['HEAD', 'GET'])) {
  return_error(405, 'Method Not Allowed', 'You can not use '.$_SERVER['REQUEST_METHOD'].' method on this page.');
}

$source_stub = $converter_matches[1][0];
$source_extension = $converter_matches[2][0];
$target_extension = $converter_matches[3][0];

$source_directory = [ 6 => 'short_names', 64 => 'images' ][strlen($source_stub)];
$source_filename = $source_stub.'.'.$source_extension;
$source_path = __DIR__.'/../data/'.$source_directory.'/'.$source_filename;

if (!file_exists($source_path)) {
  return_error(404, 'Not Found', 'The requested resource was not found on the server. If you entered the URL by hand, please check the spelling.');
}

$img = new Imagick($source_path);
$img->setImageFormat($target_extension);

$img_blob = $img->getImageBlob();
$img->destroy();

header('Content-Type: '.[
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'gif' => 'image/gif',
  'webp' => 'image/webp',
][$target_extension]);
header('Content-Length: '.strlen($img_blob));
header('Cache-Control: max-age=31536000'); // 1y

echo $img_blob;
