<?php

require_once '../lib/return_error.php';

if (!in_array($_SERVER['REQUEST_METHOD'], ['HEAD', 'GET'])) {
  return_error(405, 'Method Not Allowed', 'You can not use '.$_SERVER['REQUEST_METHOD'].' method on this page.');
}

$page_title = 'API';

$page_content = "
<p>Ymage has a programmatic interface to upload files.</p>
<p>To upload a file, send a POST request to <code>".SITE_URL."/</code>.</p>
<h2>Content-Type</h2>
<p>You can use multipart/form-data format to upload a file, which the parameter <code>file</code> contains the file content.</p>
<p>Alternatively, plain image binary (image/png, image/jpeg, image/gif, image/webp) can also be POSTed.</p>
<h2>Accept</h2>
<p>The <code>Accept</code> header in the request instructs the server which Content-Type you want to get in the response. The available values are text/html, text/plain and application/json.</p>
<pre class=\"syntax-highlight\">
<span class=\"rem\"># The text response looks like this:</span>

Short URL: ".SITE_URL."/o2rdt4.png
Long URL: ".SITE_URL."/a266fac8ac05a90e4c6d82057bf9976b5a349f0ed253abf0e46abb236750c655.png

<span class=\"rem\"># Or if there's an error:</span>

Error: Failed to upload file. Please try again.

<span class=\"rem\"># The JSON response looks like this:</span>

{
  <span class=\"key\">\"short_url\"</span>: <span class=\"str\">\"".SITE_URL."/o2rdt4.png\"</span>,
  <span class=\"key\">\"long_url\"</span>: <span class=\"str\">\"".SITE_URL."/a266fac8ac05a90e4c6d82057bf9976b5a349f0ed253abf0e46abb236750c655.png\"</span>
}

<span class=\"rem\"># Or if there's an error:</span>

{
  <span class=\"key\">\"error\"</span>: {
    <span class=\"key\">\"code\"</span>: <span class=\"num\">400</span>,
    <span class=\"key\">\"message\"</span>: <span class=\"str\">\"Failed to upload file. Please try again.\"</span>
  }
}
</pre>
</p>If the <code>Accept</code> header is not satifiable. It will return HTML for <code>User-Agent</code>s contains <code>Mozilla</code>, text otherwise.</p>
<h2>Examples</h2>
<p>These examples use cURL to demonstrate the usage of this API.</p>
<pre class=\"syntax-highlight\">
$ <span class=\"fun\">curl</span> <span class=\"opt\">-H</span> <span class=\"str\">'Accept: text/plain'</span> <span class=\"opt\">-F</span> <span class=\"str\">'file=@file-to-upload.png'</span> ".SITE_URL."/
Short URL: ".SITE_URL."/o2rdt4.png
Long URL: ".SITE_URL."/a266fac8ac05a90e4c6d82057bf9976b5a349f0ed253abf0e46abb236750c655.png

$ <span class=\"fun\">curl</span> <span class=\"opt\">-X</span> POST <span class=\"opt\">-H</span> <span class=\"str\">'Accept: application/json'</span> <span class=\"opt\">--data-binary</span> <span class=\"str\">'@file-to-upload.png'</span> ".SITE_URL."/
{
  <span class=\"key\">\"short_url\"</span>: <span class=\"str\">\"".SITE_URL."/o2rdt4.png\"</span>,
  <span class=\"key\">\"long_url\"</span>: <span class=\"str\">\"".SITE_URL."/a266fac8ac05a90e4c6d82057bf9976b5a349f0ed253abf0e46abb236750c655.png\"</span>
}
</pre>
";

header('Cache-Control: no-cache');
require '../templates/static_page.php';
