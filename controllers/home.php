<?php

require_once '../lib/return_error.php';
require_once '../lib/add_cors_headers.php';
require_once '../lib/random_string.php';
require_once '../lib/determine_output_format.php';

class UploadException extends Exception {}

switch ($_SERVER['REQUEST_METHOD']) {
  case 'HEAD':
  case 'GET':
    $upload_error = null;
    $upload_result_url_long = null;
    $upload_result_url_short = null;
    header('Cache-Control: no-cache');
    add_cors_headers();
    require '../templates/welcome.php';
    break;

  case 'OPTIONS':
    header('Cache-Control: no-cache');
    add_cors_headers();
    break;

  case 'POST':
    try {
      if (strtok($_SERVER['HTTP_CONTENT_TYPE'], ';') == 'multipart/form-data') {
        if (!array_key_exists('file', $_FILES) || !$_FILES['file']['size']) {
          throw new UploadException('No file has been chosen.', 400);
        }
        switch ($_FILES['file']['error']) {
          case UPLOAD_ERR_OK:
            break;
          case UPLOAD_ERR_INI_SIZE:
          case UPLOAD_ERR_FORM_SIZE:
            throw new UploadException('File is too large. Maximum file size is 4M.', 413);
            break;
          case UPLOAD_ERR_NO_FILE:
          case UPLOAD_ERR_PARTIAL:
            throw new UploadException('Failed to upload file. Please try again.', 400);
            break;
          case UPLOAD_ERR_NO_TMP_DIR:
          case UPLOAD_ERR_CANT_WRITE:
            throw new UploadException('The server has not been configured correctly.', 500);
            break;
          default:
            throw new UploadException('An unknown error has occured, code: '.$_FILES['file']['error'].'.', 500);
        }
        if ($_FILES['file']['size'] > 1024 * 1024 * 4) {
          throw new UploadException('File is too large. Maximum file size is 4M.', 413);
        }
        $tmp_name = $_FILES['file']['tmp_name'];
        if (!is_uploaded_file($tmp_name)) {
          throw new UploadException('Failed to upload file. Please try again.', 400);
        }

      } else {
        $input_stream = fopen('php://input', 'r');
        $tmp_name = __DIR__.'/../data/tmp/'.random_string(32);
        if (!($tmp_handle = fopen($tmp_name, 'w'))) {
          throw new UploadException('The server has not been configured correctly.', 500);
        }
        $file_size = 0;
        while ($chunk = fread($input_stream, 1024)) {
          $file_size += 1024;
          if ($file_size > 1024 * 1024 * 4) {
            fclose($input_stream);
            fclose($tmp_handle);
            unlink($tmp_name);
            throw new UploadException('File is too large. Maximum file size is 4M.', 413);
          }
          fwrite($tmp_handle, $chunk);
        }
        fclose($input_stream);
        fclose($tmp_handle);
      }

      $supported_types = ['image/png', 'image/jpeg', 'image/gif', 'image/webp'];
      $file_type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tmp_name, FILEINFO_MIME_TYPE);
      if (!in_array($file_type, $supported_types)) {
        throw new UploadException('File format must be PNG, JPEG, GIF or WebP.', 400);
      }

      $hash = hash_file('sha256', $tmp_name);
      $name_extention = '.'.[
        'image/png' => 'png',
        'image/jpeg' => 'jpg',
        'image/gif' => 'gif',
        'image/webp' => 'webp',
      ][$file_type];

      if (file_exists('../data/images/'.$hash.$name_extention)) {
        $upload_result_url_long = SITE_URL.'/'.$hash.$name_extention;
        $upload_result_url_short = SITE_URL.'/'.file_get_contents('../data/long_to_short/'.$hash);
        $upload_error = null;

      } else {
        if (!rename($tmp_name, '../data/images/'.$hash.$name_extention)) {
          throw new UploadException('The server has not been configured correctly.', 500);
        }
        chmod('../data/images/'.$hash.$name_extention, 0644);

        $short_name_generator_info_file = fopen('../data/short_name_generator_info', 'c+');
        if (!$short_name_generator_info_file) {
          unlink('../data/images/'.$hash.$name_extention);
          throw new UploadException('The server has not been configured correctly.', 500);
        }
        flock($short_name_generator_info_file, LOCK_EX);
        $short_name_generator_info = json_decode(fread($short_name_generator_info_file, filesize('../data/short_name_generator_info') ?: 1), true);
        if (!$short_name_generator_info) {
          $short_name_generator_info = [
            'sequence' => 0,
            'salt' => random_string(32),
          ];
        }
        $short_name_generator_info['sequence']++;
        ftruncate($short_name_generator_info_file, 0);
        rewind($short_name_generator_info_file);
        fwrite($short_name_generator_info_file, json_encode($short_name_generator_info));
        flock($short_name_generator_info_file, LOCK_UN);
        fclose($short_name_generator_info_file);
        $short_name = (new Hashids\Hashids($short_name_generator_info['salt'], 6))->encode($short_name_generator_info['sequence']);
        if (!symlink('../images/'.$hash.$name_extention, '../data/short_names/'.$short_name.$name_extention)) {
          unlink('../data/images/'.$hash.$name_extention);
          throw new UploadException('The server has not been configured correctly.', 500);
        }
        if (file_put_contents('../data/long_to_short/'.$hash, $short_name.$name_extention) === false) {
          unlink('../data/images/'.$hash.$name_extention);
          unlink('../data/short_names/'.$short_name.$name_extention);
          throw new UploadException('The server has not been configured correctly.', 500);
        }

        $upload_result_url_long = SITE_URL.'/'.$hash.$name_extention;
        $upload_result_url_short = SITE_URL.'/'.$short_name.$name_extention;
        $upload_error = null;
      }

    } catch (UploadException $e) {
      $upload_result_url_long = null;
      $upload_result_url_short = null;
      $upload_error = $e->getMessage();
      http_response_code($e->getCode());
    }

    if (file_exists($tmp_name)) {
      unlink($tmp_name);
    }

    header('Cache-Control: no-cache');
    add_cors_headers();

    switch(determine_output_format()) {
      case 'html':
        require '../templates/welcome.php';
        break;
      case 'json':
        header('Content-Type: application/json');
        if ($upload_error) {
          echo json_encode([
            'error' => [
              'code' => http_response_code(),
              'message' => $upload_error,
            ],
          ]);
        } else {
          echo json_encode([
            'short_url' => $upload_result_url_short,
            'long_url' => $upload_result_url_long,
          ]);
        }
        break;
      case 'text':
        header('Content-Type: text/plain');
        if ($upload_error) {
          echo 'Error: '.$upload_error."\n";
        } else {
          echo 'Short URL: '.$upload_result_url_short."\n"
            .'Long URL: '.$upload_result_url_long."\n";
        }
        break;
    }

    break;

  default:
    return_error(405, 'Method Not Allowed', 'You can not use '.$_SERVER['REQUEST_METHOD'].' method on this page.');
}
