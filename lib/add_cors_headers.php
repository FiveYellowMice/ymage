<?php

function add_cors_headers() {
  if (!CORS_ALLOWED_ORIGINS) return;

  if (CORS_ALLOWED_ORIGINS === '*') {
    header('Access-Control-Allow-Origin: *');
  } elseif (
    array_key_exists('HTTP_ORIGIN', $_SERVER) &&
    preg_match(CORS_ALLOWED_ORIGINS, $_SERVER['HTTP_ORIGIN'])
  ) {
    header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
    header('Vary: Origin');
  } else {
    return;
  }

  header('Access-Control-Allow-Headers: Accept, Content-Type');
  header('Access-Control-Allow-Methods: GET, HEAD, OPTIONS, POST');
}
