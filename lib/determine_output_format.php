<?php

function determine_output_format() {
  $output_format = null;
  if (array_key_exists('HTTP_ACCEPT', $_SERVER)) {
    if (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') !== false) {
      $output_format = 'html';
    } elseif (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
      $output_format = 'json';
    } elseif (strpos($_SERVER['HTTP_ACCEPT'], 'text/plain') !== false) {
      $output_format = 'text';
    }
  }
  if (!$output_format) {
    if (array_key_exists('HTTP_USER_AGENT', $_SERVER) && strpos($_SERVER['HTTP_USER_AGENT'], 'Mozilla') !== false) {
      $output_format = 'html';
    } else {
      $output_format = 'text';
    }
  }
  return $output_format;
}
