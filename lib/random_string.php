<?php

function random_string($length) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $result = '';
  for ($i = 0; $i < $length; $i++) {
    $result .= $characters[rand(0, 61)];
  }
  return $result;
}
