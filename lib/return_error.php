<?php

function return_error($code, $message = null, $explaination = null) {
  http_response_code($code);
  header('Cache-Control: no-cache');
  require '../templates/error.php';
  die();
}
