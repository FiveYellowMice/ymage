# Ymage

Ymage pastebin.

## Setup

Prerequisites:
- Nginx
- PHP-FPM >= 5.6
- PHP extension PCRE
- PHP extension BC Math
- PHP extension Imagick

First, visit [this page](https://gitlab.com/FiveYellowMice/ymage/tags) and download the latest release.

Extract files.

`cp config.example.php config.php`, then change the values inside.

Change the owner of the `data` directory to the user that PHP-FPM runs as. For example, `chown -R apache:apache data`. If you use SELinux, also changes its context to `httpd_sys_rw_content_t`.

Setup Nginx and PHP-FPM, set the root directive to the `public` directory you have extrated.

Add the following rules, change `/srv/img` to other values as you need:

```nginx
client_max_body_size 4M;

location / {
  try_files $uri $uri/ /index.php;
  rewrite "^/([a-zA-Z0-9]{6}\.(?:png|jpg|gif|webp))$" /short_names/$1;
  rewrite "^/([a-f0-9]{64}\.(?:png|jpg|gif|webp))$" /images/$1;

  if ($request_method = OPTIONS) {
    rewrite ^ /index.php;
  }
}

location /images/ {
  internal;
  alias /srv/img/data/images/;
  try_files $uri $uri/ /index.php;
  expires 1y;
}

location /short_names/ {
  internal;
  alias /srv/img/data/short_names/;
  try_files $uri $uri/ /index.php;
  expires 1y;
}

location /node_modules/ {
  alias /srv/img/node_modules/;
}
```

Then you are good to go.
