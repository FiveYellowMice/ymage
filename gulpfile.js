'use strict'
const gulp = require('gulp')
const sass = require('gulp-sass')
const postcss = require('gulp-postcss')
const autoprefixer = require('autoprefixer')
const cleanCSS = require('gulp-clean-css')


gulp.task('compile:css', function() {
  gulp.src(['assets/css/*.scss', '!assets/css/_*.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([
      autoprefixer({
        browsers: [
          'IE 10',
          'iOS 8',
          'Chrome >= 38',
          'Firefox ESR'
        ]
      })
    ]))
    .pipe(cleanCSS())
    .pipe(gulp.dest('public/assets/css'))
})

gulp.task('compile', ['compile:css'])

gulp.task('watch', ['compile'], function() {
  gulp.watch('assets/css/*.scss', ['compile:css'])
})

gulp.task('default', ['compile:css'])
