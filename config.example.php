<?php

/**
 * Site's URL, without trailing slash.
 * Examples:
 *   https://example.com
 *   https://example.com:8080
 */
define('SITE_URL', 'https://example.com');


/**
 * Allowed origins for CORS.
 * Available values are:
 *   '*'   - Any origin
 *   false - Disable CORS
 *         - Any PCRE RegEx
 */
define('CORS_ALLOWED_ORIGINS', '/^https:\/\/example\.org$/');
