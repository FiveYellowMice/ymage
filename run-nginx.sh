#!/bin/sh

rm -f run/nginx.pid

cat <<-EOF > run/nginx.conf
daemon off;
master_process off;
pid "$(pwd)/run/nginx.pid";
error_log /proc/self/fd/1;

events {}

http {
  client_body_temp_path "$(pwd)/run/client_body";
  fastcgi_temp_path "$(pwd)/run/fastcgi_temp";
  proxy_temp_path "$(pwd)/run/proxy_temp";
  scgi_temp_path "$(pwd)/run/scgi_temp";
  uwsgi_temp_path "$(pwd)/run/uwsgi_temp";

  access_log /proc/self/fd/1;
  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  charset UTF-8;

  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  upstream php-fpm {
    server "unix:$(pwd)/run/php-fpm.sock";
  }

  server {
    listen 8087;
    listen [::]:8087;
    root "$(pwd)/public";

    client_max_body_size 4M;

    index index.php index.html index.htm;

    location / {
      try_files \$uri \$uri/ /index.php;
      rewrite "^/([a-zA-Z0-9]{6}\.(?:png|jpg|gif|webp))\$" /short_names/\$1;
      rewrite "^/([a-f0-9]{64}\.(?:png|jpg|gif|webp))\$" /images/\$1;

      if (\$request_method = OPTIONS) {
        rewrite ^ /index.php;
      }
    }

    location /images/ {
      internal;
      alias "$(pwd)/data/images/";
      try_files \$uri \$uri/ /index.php;
      expires 1y;
    }

    location /short_names/ {
      internal;
      alias "$(pwd)/data/short_names/";
      try_files \$uri \$uri/ /index.php;
      expires 1y;
    }

    location /node_modules/ {
      alias "$(pwd)/node_modules/";
    }

    location ~ \.php$ {
      try_files \$uri =404;
      fastcgi_intercept_errors on;
      fastcgi_index  index.php;
      include        /etc/nginx/fastcgi_params;
      fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
      fastcgi_param  HTTP_PROXY "";
      fastcgi_pass   php-fpm;
    }
  }
}
EOF

nginx -c "$(pwd)/run/nginx.conf"

rm -f run/nginx.pid
rm run/nginx.conf
