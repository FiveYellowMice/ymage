default: compile_assets

GIT_REVISION = $(shell git rev-parse HEAD | head -c7)

composer:
	composer install

npm:
	npm install --development

compile_assets: npm
	gulp

package: composer compile_assets
	rm -rf dist/ymage-$(GIT_REVISION)
	mkdir -p dist/ymage-$(GIT_REVISION)
	cp -R \
		vendor \
		controllers \
		lib \
		public \
		templates \
		config.example.php \
		LICENSE \
		README.md \
		dist/ymage-$(GIT_REVISION)/
	mkdir -p \
		dist/ymage-$(GIT_REVISION)/data/images \
		dist/ymage-$(GIT_REVISION)/data/short_names \
		dist/ymage-$(GIT_REVISION)/data/long_to_short \
		dist/ymage-$(GIT_REVISION)/data/tmp \
		dist/ymage-$(GIT_REVISION)/node_modules/jquery \
		dist/ymage-$(GIT_REVISION)/node_modules/material-design-icons
	cp -R node_modules/jquery/dist dist/ymage-$(GIT_REVISION)/node_modules/jquery/
	cp -R node_modules/material-design-icons/iconfont dist/ymage-$(GIT_REVISION)/node_modules/material-design-icons/
	cd dist; tar -acf ymage-$(GIT_REVISION).tar.gz ymage-$(GIT_REVISION)
	rm -rf dist/ymage-$(GIT_REVISION)

clear_data:
	rm -f data/images/*
	rm -f data/short_names/*
	rm -f data/long_to_short/*
	rm -f data/tmp/*
	rm -f data/short_name_generator_info
