<?php

require '../vendor/autoload.php';
require_once '../lib/return_error.php';

require '../config.php';

$request_path = strtok($_SERVER['REQUEST_URI'], '?');

switch ($request_path) {
  case '/':
    require '../controllers/home.php';
    break;

  case '/about':
    require '../controllers/about.php';
    break;

  case '/api':
    require '../controllers/api.php';
    break;

  default:
    if (preg_match(
      '/^\/([a-zA-Z0-9]{6}|[a-f0-9]{64})\.(png|jpg|gif|webp)\.(png|jpg|gif|webp)$/',
      $request_path, $converter_matches, PREG_OFFSET_CAPTURE
    )) {
      require '../controllers/converter.php';
    } else {
      return_error(404, 'Not Found', 'The requested resource was not found on the server. If you entered the URL by hand, please check the spelling.');
    }
}
