#!/bin/sh

exit_hook() {
  kill -TERM $(cat run/php-fpm.pid)
  rm -f run/php-fpm.sock
  rm -f run/php-fpm.pid
  rm run/php-fpm.conf
}

rm -f run/php-fpm.sock
rm -f run/php-fpm.pid

cat <<-EOF > run/php-fpm.conf
[global]
error_log = /proc/self/fd/2
pid = "$(pwd)/run/php-fpm.pid"

[www]
access.log = /proc/self/fd/2
listen = "$(pwd)/run/php-fpm.sock"
pm = static
pm.max_children = 1
EOF

php-fpm --daemonize --fpm-config "$(pwd)/run/php-fpm.conf"

trap exit_hook SIGINT SIGTERM
sleep infinity
